<?php
    declare(strict_types=1);
    header('HTTP/1.1 307 Temporary Redirect');
    header('Location: http://example.com/redirected');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    別のサイトにリダイレクトします。
</body>
</html>