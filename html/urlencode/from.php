<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <a href = "to.php?message=ア&イ&ウ&エ&オ">URLエンコードしないリンク</a><br>
    <a href = "to.php?message=<?=urlencode('ア&イ&ウ&エ&オ')?>">URLエンコードしたリンク</a>
</body>
</html>