<?php
    declare(strict_types=1);

    require_once dirname(__FILE__) . '/functions.php';

    //メインルーチン
    try {
        if (!isset($_POST['title']) || trim($_POST['title']) === '') {
            header('Location: http://localhost/myblogs/main.php');
            return;
        }
        $id = intval($_POST['id']);
        $pdo = connect();
        $statement = $pdo->prepare('UPDATE blogs SET title = :title, content = :content, published = :published WHERE id = :id');
        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        $statement->bindValue(':title', $_POST['title'], PDO::PARAM_STR);
        $statement->bindValue(':content', $_POST['content'], PDO::PARAM_STR);
        $statement->bindValue(':published', date("Y-m-d"), PDO::PARAM_STR);
        $statement->execute();
    } catch (PDOException $e) {
        var_dump($e);
        return;
    }
    header('Location: http://localhost/myblogs/main.php');