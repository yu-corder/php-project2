<?php
    declare(strict_types=1);

    require_once dirname(__FILE__) . '/functions.php';

    //メインルーチン
    try {
        if (!isset($_POST['title']) || trim($_POST['title']) === '') {
            return;
        }
        $pdo = connect();
        $statement = $pdo->prepare('INSERT INTO blogs(title, content, published) VALUES(:title, :content, :published)');
        $statement->bindValue(':title', $_POST['title'], PDO::PARAM_STR);
        $statement->bindValue(':content', $_POST['content'], PDO::PARAM_STR);
        $statement->bindValue(':published', date("Y-m-d"), PDO::PARAM_STR);
        $statement->execute();
    } catch (PDOException $e) {
        return;
    }
    header('Location: http://localhost/myblogs/main.php');
