<?php
    declare(strict_types=1);

    require_once dirname(__FILE__) . '/functions.php';

    //メインルーチン
    try {
        if (!isset($_GET['id']) || $_GET['id'] === '') {
            header('Location: http://localhost/myblogs/main.php');
        }
        $id = intval($_GET['id']);
        $pdo = connect();
        $statement = $pdo->prepare('DELETE FROM blogs WHERE id = :id');
        $statement->bindValue(':id', $id, PDO::PARAM_INT);
        $statement->execute();
    } catch (PDOException $e) {
        var_dump($e);
        return;
    }
    header('Location: http://localhost/myblogs/main.php');