<?php
    declare(strict_types=1);

    require_once dirname(__FILE__) . '/functions.php';

    if (!isset($_GET['id']) || $_GET['id'] === '') {
        header('Location: http://localhost/myblogs/main.php');
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h3>ブログ入力ページ</h3>
    <form name="blog-form" action="update.php" method="POST">
        <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
        <input type="text" name="title" value=""><br>
        <textarea name="content"></textarea><br>
        <button type="submit" name="operation" value="send">送信</button>
    </form>
</body>
</html>