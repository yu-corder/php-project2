<?php
    declare(strict_types=1);

    function connect(): PDO
    {
        $pdo = new PDO('mysql:host=mysql; dbname=blog; charset=utf8mb4', 'root', 'root');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        return $pdo;
    }

    function escape($value)
    {
        return htmlspecialchars(strval($value), ENT_QUOTES | ENT_HTML5, 'UTF-8');
    }