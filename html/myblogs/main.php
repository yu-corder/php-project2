<?php
    declare(strict_types=1);

    require_once dirname(__FILE__) . '/functions.php';

    //メインルーチン
    try {
        $pdo = connect();
        $statement = $pdo->prepare('SELECT * FROM blogs');
        $statement->execute();
    } catch (PDOException $e) {
        echo 'ブログの表示に失敗しました。';
        return;
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h3>俺のブログ!</h3>
    <a href = "http://localhost/myblogs/form.html">ブログ投稿画面へ</a>
    <?php while ($row = $statement->fetch(PDO::FETCH_ASSOC)): ?>
        <div>
            <p><?=escape($row['title'])?></p>
            <p><?=escape($row['content'])?></p>
            <p><?=escape($row['published'])?></p>
            <a href = "http://localhost/myblogs/edit.php?id=<?=escape($row['id'])?>">編集</a>
            <a href = "http://localhost/myblogs/delete.php?id=<?=escape($row['id'])?>">削除</a>
        </div>
    <?php endwhile; ?>
</body>
</html>