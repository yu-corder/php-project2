<?php declare(strict_types=1); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <h1>webブラウザから送信されたクッキーの内容は以下のとおりです。</h1>
    <pre><?php print_r($_COOKIE)?></pre>
</body>
</html>