<?php
    declare(strict_types=1);
    for($i = 1; $i <= 500; $i++) {
        setcookie("name[{$i}]", "value[{$i}]", time() + 3600, '/', '', false, true);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>Hello World -PHP-</title>
</head>
<body>
    <p>PHPからクッキーを創出しました。</p>
</body>
</html>