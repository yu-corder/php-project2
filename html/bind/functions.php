<?php
    declare(strict_types=1);

    function connect()
    {
        $pdo = new PDO('mysql:host=mysql; dbname=honkaku; charset=utf8mb4', 'root', 'root');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        return $pdo;
    }