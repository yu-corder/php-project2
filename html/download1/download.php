<?php
    declare(strict_types=1);
    $pdfFiles = [
        1 => 'sample1.txt',
        2 => 'sample2.txt',
        3 => 'sample3.txt'
    ];
    if (!isset($_GET['type']) || !isset($pdfFiles[$_GET['type']])) {
        exit;
    }
    $file = $pdfFiles[$_GET['type']];
    header('Content-Length: '. filesize($file));
    header('Content-Type: application/octer-strem');
    header('Content-Disposition: attachment; filename=' . $file);
    readfile($file);