<?php
declare(strict_types=1);
$images = [
    'circle' => 'hourglass-620397_1920.jpg',
    'triangle' => 'mediterranean-cuisine-2378758_1920.jpg'
];
if (!isset($_GET['type']) || !isset($images[$_GET['type']])) {
    exit;
}
$file = $images[$_GET['type']];
header('Content-Length: ' . filesize($file));
header('Content-Type: image/png');
readfile($file);